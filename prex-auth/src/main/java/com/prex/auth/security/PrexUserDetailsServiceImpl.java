package com.prex.auth.security;

import cn.hutool.core.util.ObjectUtil;
import com.prex.auth.feign.RemoteUserService;
import com.prex.base.api.dto.UserDetailsInfo;
import com.prex.base.api.entity.SysUser;
import com.prex.common.auth.service.LoginType;
import com.prex.common.auth.service.PrexSecurityUser;
import com.prex.common.core.utils.R;
import com.prex.common.social.entity.GiteeUserInfo;
import com.prex.common.social.service.GiteeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Set;

/**
 * @Classname UserServiceDetail
 * @Description 用户身份验证
 * @Author 李号东 im.lihaodong@gmail.com
 * @Date 2019-03-19 16:55
 * @Version 1.0
 */
@Slf4j
@Service
public class PrexUserDetailsServiceImpl implements UserDetailsService, SocialUserDetailsService {

    @Autowired
    private RemoteUserService remoteUserService;

    @Autowired
    private GiteeService giteeService;

    /**
     * 通过用户名查询用户
     *
     * @param username
     * @return
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        R<UserDetailsInfo> info = remoteUserService.info(username);

        if (ObjectUtil.isNull(info) || info.getCode() == HttpStatus.NOT_FOUND.value()) {
            log.debug("登录用户：" + username + " 不存在.");
            throw new UsernameNotFoundException("登录用户：" + username + " 不存在");
        }
        UserDetailsInfo userDetailsInfo = info.getData();
        SysUser user = userDetailsInfo.getSysUser();
        Set<String> permissions = userDetailsInfo.getPermissions();
        Collection<? extends GrantedAuthority> authorities = AuthorityUtils.createAuthorityList(permissions.toArray(new String[0]));
        return new PrexSecurityUser(user.getUserId(), username, "{bcrypt}"+user.getPassword(), authorities);

    }

    /**
     * 手机验证码登录
     *
     * @param mobile
     * @return
     * @throws UsernameNotFoundException
     */
    public UserDetails loadUserByMobile(String mobile) throws UsernameNotFoundException {
        //  通过手机号mobile去数据库里查找用户以及用户权限
        R<UserDetailsInfo> info = remoteUserService.info(mobile);
        if (info.getCode() == HttpStatus.NOT_FOUND.value()) {
            log.info("登录手机号：" + mobile + " 不存在.");
            throw new UsernameNotFoundException("登录手机号：" + mobile + " 不存在");
        }
        UserDetailsInfo userDetailsInfo = info.getData();
        SysUser user = userDetailsInfo.getSysUser();
        Set<String> permissions = userDetailsInfo.getPermissions();
        // 获取用户拥有的角色
        Collection<? extends GrantedAuthority> authorities = AuthorityUtils.createAuthorityList(permissions.toArray(new String[0]));
        return new PrexSecurityUser(user.getUserId(), user.getUsername(), "{bcrypt}"+user.getPassword(), authorities);
    }

    @Override
    public PrexSecurityUser loadUser(String code, LoginType loginType) {
        String type = loginType.getDescription();
        GiteeUserInfo userInfo = giteeService.getUserInfo(code);
        R<UserDetailsInfo> info = remoteUserService.getUserInfoBySocial(type, userInfo.getId());

        System.out.println(info);
        if (info.getCode() == HttpStatus.NOT_FOUND.value()) {
            log.info("登录社交Id：" + userInfo.getId() + " 不存在.");
            throw new UsernameNotFoundException("登录手机号：" + userInfo.getId() + " 不存在");
        }
        UserDetailsInfo userDetailsInfo = info.getData();

        SysUser user = userDetailsInfo.getSysUser();
        Set<String> permissions = userDetailsInfo.getPermissions();
        // 获取用户拥有的角色
        Collection<? extends GrantedAuthority> authorities = AuthorityUtils.createAuthorityList(permissions.toArray(new String[0]));
        return new PrexSecurityUser(user.getUserId(), user.getUsername(), "{bcrypt}"+user.getPassword(), authorities);
    }
}
