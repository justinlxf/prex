package com.prex.common.social.config;

import org.springframework.web.client.RestTemplate;

/**
 * @Classname SocialRestTemplate
 * @Description TODO
 * @Author Created by Lihaodong (alias:小东啊) im.lihaodong@gmail.com
 * @Date 2019-09-16 15:05
 * @Version 1.0
 */
public class SocialRestTemplate {
    private static RestTemplate restTemplate;
    public static RestTemplate getRestTemplate(){
        if(restTemplate == null){
            restTemplate = new RestTemplate();
        }
        return restTemplate;
    }
}